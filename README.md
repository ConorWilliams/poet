# POET

Potential Optimization by Evolutionary Techniques (POET)

## Running POET
Follow these steps to run POET
1. Clone the repository 
```
git clone https://gitlab.com/muellergroup/poet.git
```
2. Create the directory /yourPath/poet/poet_run/runs/
3. Create the directory /yourPath/poet/poet_run/runs/yourRunName/
4. Copy the files in /yourPath/poet/poet_run/exampleRun/ to /yourPath/poet/poet_run/runs/yourRunName/
5. Change directory to /yourPath/poet/poet_run/runs/yourRunName/
6. Edit the inputPM.txt file as required (see the comments in that file)
7. Run the command
```
python run_restart_results.py run
```
8. When the run ends, the file Completed_all_instances.log will be written to /yourPath/poet/poet_run/runs/yourRunName/ with details about how each instance ended. The python script is likely to return before POET actually completes.

Note: if you want to check the log information of each instance, then you can view the file /yourPath/poet/poet_run/runs/yourRunName/n/log.java.run, where n = 1,..., number of instances


## Writing the results of an archived run
Note: you need to have an archived run in the directory /yourPath/poet/poet_run/runs/yourRunName/archived_run_number_n. If you need to archive a run, then follow the instructions in the section "Archiving a run".

Follow these steps to write the results of POET
1. Change directory to /yourPath/poet/poet_run/runs/yourRunName/
2. Run the command
```
python run_restart_results.py results archived_run_number_n
```
3. View the file /yourPath/poet/poet_run/runs/yourRunName/archived_run_number_n/results/yourRunName_convexHull.pdf

Where archived_run_number_n is the name of the directory that contains the archived run. So, archived_run_number_n is the base name of this directory /yourPath/poet/poet_run/runs/yourRunName/archived_run_number_n


## Archiving a run
Follow these steps to archive the run /yourPath/poet/poet_run/runs/yourRunName/ 
1. Change directory to /yourPath/poet/poet_run/runs/yourRunName/
2. Run the command
```
python run_restart_results.py archive 
```

This creates an archive /yourPath/poet/poet_run/runs/yourRunName/archived_run_number_n, where n is an integer that represents the latest archive. Please do not change the naming convention of archived_run_number_n because the script run_restart_results.py uses it to determine n for archived_run_number_n

## Restarting an archived run
Note: you need to have an archived run in the directory /yourPath/poet/poet_run/runs/yourRunName/archived_run_number_n. If you need to archive a run, then follow the instructions in the section "Archiving a run".

Follow these steps to restart an archived the run /yourPath/poet/poet_run/runs/yourRunName/
1. Change directory to /yourPath/poet/poet_run/runs/yourRunName/
2. Run the command
```
python run_restart_results.py restart archived_run_number_n
```

Where archived_run_number_n is the name of the directory that contains the archived run. So, archived_run_number_n is the base name of this directory /yourPath/poet/poet_run/runs/yourRunName/archived_run_number_n
