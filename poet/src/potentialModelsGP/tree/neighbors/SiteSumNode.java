/* Created on Aug 10, 2016
*
*/

package potentialModelsGP.tree.neighbors;

import java.util.ArrayList;

import evaluator.NotFiniteValueException;
import evaluator.StructureNet;
import evaluator.StructureNet.Bond;
import potentialModelsGP.tree.ConstantNode;
import potentialModelsGP.tree.Node;

public class SiteSumNode extends Node {
 
 private boolean m_UseFirstSite;
// private Boolean m_HasDistance = null;
 private static double m_rout = Double.NaN;
 private static double m_rin  = Double.NaN;
 private static final char m_Smooth = 's';
 
 public SiteSumNode() {
   super(new Node[0]);
 }
 
 public SiteSumNode(Node childNode, boolean useFirstSite) {
   super(new Node[] {childNode});
   //TODO
   //m_UseFirstSite = true can be substituted by multiples of m_UseFirstSite = false
   //Therefore, it is more computationally efficient to use only m_UseFirstSite = false and
   //let the algorithm include multiples of the SiteSumNode if necessary
//   m_UseFirstSite = false;
   m_UseFirstSite = useFirstSite;
 }

 @Override
 public Node generateNode(Node childNode) {
   return new SiteSumNode(childNode, GENERATOR.nextBoolean());
 }

 @Override
 public double calculateValue(Bond bond, matsci.structure.Structure.Site definingSite, Integer axis, Double daxis) throws NotFiniteValueException {
	//'n' for no smoothing, 'e' for exponential, 's' for spline
	 char smoothing = m_Smooth;
//	 if(m_HasDistance == null) {
//		 TreePotential tree = new TreePotential(this.deepCopy(), 5.5);
//		 m_HasDistance = tree.hasDistance(); 
//	 }
//	 if(!m_HasDistance) {
//		 smoothing = 'c';
//	 }
	 StructureNet.Site site = this.getUseFirstSite() ? bond.getCentralSiteOfBond() : bond.getNeighborSiteOfBond();
	 
//	 From https://doi.org/10.1016/0009-2614(92)85708-I Hong-Quiang Ding, Naoki Karasawa and William Goddard, Chemical Physics Letters, Volume 193, number 1,2,3. Year 1992
	 if(smoothing == 's'){
		 double returnValue = 0;
		 double rin = getRin();
		 double rout = getRout();
		 double rinsq = rin*rin;
		 double routsq = rout*rout;
		 double t3 = (routsq-rinsq)*(routsq-rinsq)*(routsq-rinsq);
		 for (int neighborNum = 0; neighborNum < site.numNeighbors(); neighborNum++) {   
			 StructureNet.Bond neighborBond = site.getBond(neighborNum);
			 double r = neighborBond.getBondLength(definingSite, axis, daxis);
			 double s;
			 if(r <= rin){s=1.0;}
			 else if(r >= rout){s = 0.0;}
			 else{
				 double rsq = r*r;
				 double t1 = (routsq-rsq)*(routsq-rsq);
				 double t2 = (routsq+2*rsq-3*rinsq);
				 s = (t1*t2)/t3;
			 }
			 returnValue += (m_ChildNodes[0].getValue(neighborBond, definingSite, axis, daxis)*s);
		 }
		 return returnValue;  
	 }
	 
//	 //smoothing(x) = 6x^5 - 15x^4 +10x^3
//	 //where x = (r_outer - r) / (r_outer - r_inner)
//	 if(smoothing == 's'){
//		 double returnValue = 0;
//		 double rin = getRin();
//		 double rout = getRout();
//
//		 
//		 for (int neighborNum = 0; neighborNum < site.numNeighbors(); neighborNum++) {   
//			 StructureNet.Bond neighborBond = site.getBond(neighborNum);
//			 double r = neighborBond.getBondLength(definingSite, axis, daxis);
//			 double s;
//			 if(r <= rin){s=1.0;}
//			 else if(r >= rout){s = 0.0;}
//			 else{
//				 double x = (rout-r)/(rout-rin);
//				 double xp3 = x*x*x;
//				 double xp4 = xp3*x;
//				 double xp5 = xp4*x;
//				 s = 6.0*xp5 - 15.0*xp4 + 10.0*xp3;
//			 }
//			 returnValue += (m_ChildNodes[0].getValue(neighborBond, definingSite, axis, daxis)*s);
//		 }
//		 return returnValue;  
//	 }
	 
//	 //smoothing(x) = x^4 / (1 + x^4)
//	 //where x = (r-rout) / h
//	 //Source DOI: https://doi.org/10.1088/1361-651X/ab0d75, Uncertainty quantification for classical effective potentials:an extension to POTFIT
//	 if(smoothing == 's'){
//		 double returnValue = 0;
//		 double rout = getRout();
//		 double h = 0.75;
//		 
//		 for (int neighborNum = 0; neighborNum < site.numNeighbors(); neighborNum++) {   
//			 StructureNet.Bond neighborBond = site.getBond(neighborNum);
//			 double r = neighborBond.getBondLength(definingSite, axis, daxis);
//			 double s;
//			 if(r >= rout){s = 0.0;}
//			 else{
//				 double x = (r-rout) / h;
//				 double p1 = x*x*x*x;
//				 s =  p1 / (1.0 + p1);
//			 }
//			 returnValue += (m_ChildNodes[0].getValue(neighborBond, definingSite, axis, daxis)*s);
//		 }
//		 return returnValue;  
//	 }
	 
	 else if(smoothing == 'n'){
		 double returnValue = 0;
		 for (int neighborNum = 0; neighborNum < site.numNeighbors(); neighborNum++) {   
			 returnValue += m_ChildNodes[0].getValue(site.getBond(neighborNum), definingSite, axis, daxis);
		 }
		 return returnValue;
	 }
	 else if(smoothing == 'e'){
		 double returnValue = 0;
//			Cutoff from University of Virginia, MSE 4270/6270: Introduction to Atomistic Simulations, Leonid Zhigilei
		 double c = 1.0;
		 double rc = 5.5;
		 for (int neighborNum = 0; neighborNum < site.numNeighbors(); neighborNum++) {   
			 double r = site.getBond(neighborNum).getBondLength(definingSite, axis, daxis);
			 returnValue += (m_ChildNodes[0].getValue(site.getBond(neighborNum), definingSite, axis, daxis)*Math.exp(c/(r-rc)));
		 }
		 return returnValue;  
	 }
//	 If it has no distance, then return the number of neighbors times the value
	 else if(smoothing == 'c'){
		 double returnValue = 0;
		 int numNeighbors = site.numNeighbors();
		 if(numNeighbors != 0) {
//			 This does not depend on the distance node, so use bond 0 or any other because it should give the same value
			 return numNeighbors*m_ChildNodes[0].getValue(site.getBond(0), definingSite, axis, daxis);
		 }
		 return returnValue;
	 }
	 else{
		 return Double.NaN;
	 }

 }

 @Override
 public double calculateDerivative(Bond bond, ConstantNode constant, matsci.structure.Structure.Site definingSite, Integer axis) throws NotFiniteValueException {
	//'n' for no smoothing, 'e' for exponential, 's' for spline
	 char smoothing = m_Smooth;
//	 if(m_HasDistance == null) {
//		 TreePotential tree = new TreePotential(this.deepCopy(), 5.5);
//		 m_HasDistance = tree.hasDistance(); 
//	 }
//	 if(!m_HasDistance) {
//		 if(constant == null) {return 0.0;}
//		 smoothing = 'n';
//	 }
//	Sum is the sum of all ai from i = 1 to i = n = numNeighbors
//	d(Sum)/dvar = d(a1+a2+a3+...+an)/dvar. Where var stands for variable
//	= da1/dvar + da2/dvar +...+ dan/dvar
	StructureNet.Site site = this.getUseFirstSite() ? bond.getCentralSiteOfBond() : bond.getNeighborSiteOfBond();
	double returnValue = 0;
	
	if(smoothing == 's') {
//		 From https://doi.org/10.1016/0009-2614(92)85708-I Hong-Quiang Ding, Naoki Karasawa and William Goddard, Chemical Physics Letters, Volume 193, number 1,2,3. Year 1992
		double rin = getRin();
		double rout = getRout();
		double rinsq = rin*rin;
		double routsq = rout*rout;
		double d1 = routsq-rinsq;
		double t3 = d1*d1*d1;
		for (int neighborNum = 0; neighborNum < site.numNeighbors(); neighborNum++) {
			 double r = site.getBond(neighborNum).getBondLength();
			 double rsq = r*r;
			 double s;
			 if(r <= rin){s=1.0;}
			 else if(r >= rout){s = 0.0;}
			 else{
				 double d2 = routsq-rsq;
				 double t1 = d2*d2;
				 double t2 = (routsq+2*rsq-3*rinsq);
				 s = (t1*t2)/t3;
			 }

			Bond neighborBond = site.getBond(neighborNum);
			double derchild = m_ChildNodes[0].getDerivative(site.getBond(neighborNum), constant, definingSite, axis);
			double term0 = derchild*s;
			double term1;
//			if(r <= rin), then there is no smoothing function, so the derivative of this SiteSumNode is the derivative of
//			its childNode. See that: s = 1.0 when r<=rin, so term0 = derchild*s = derchild
//			Or if constant is not null, term1=0 because the derivative of the spline with respect to any constant
//			in the tree is 0
			if(r <= rin || constant!=null){term1 = 0.0;}
			else {
				double valuechild =  m_ChildNodes[0].getValue(site.getBond(neighborNum), definingSite, axis, null);
				double bondDerivative = neighborBond.getDerivative(constant, definingSite, axis);
				double numerator = 12*r*(rsq-rinsq)*(rsq-routsq);
				double d3 = rinsq-routsq;
				double denominator = d3*d3*d3;
				double derivative0 = -numerator/denominator;
				double derspline = derivative0*bondDerivative;
				term1 = derspline*valuechild;
			}
			returnValue += (term0 + term1);
		}
		return returnValue;
	}
	
	
//	 //f(x) = 6x^5 - 15x^4 +10x^3
//	 //where x = (r_outer - r) / (r_outer - r_inner)
//	if(smoothing == 's') {
//		double rin = getRin();
//		double rout = getRout();
//
//		for (int neighborNum = 0; neighborNum < site.numNeighbors(); neighborNum++) {
//			 double r = site.getBond(neighborNum).getBondLength();
//			 double s;
//			 if(r <= rin){s=1.0;}
//			 else if(r >= rout){s = 0.0;}
//			 else{
//				 double x = (rout-r)/(rout-rin);
//				 double xp3 = x*x*x;
//				 double xp4 = xp3*x;
//				 double xp5 = xp4*x;
//				 s = 6.0*xp5 - 15.0*xp4 + 10.0*xp3;
//			 }
//
//			Bond neighborBond = site.getBond(neighborNum);
//			double derchild = m_ChildNodes[0].getDerivative(site.getBond(neighborNum), constant, definingSite, axis);
//			double term0 = derchild*s;
//			double term1;
////			if(r <= rin), then there is no smoothing function, so the derivative of this SiteSumNode is the derivative of
////			its childNode. See that: s = 1.0 when r<=rin, so term0 = derchild*s = derchild
////			Or if constant is not null, term1=0 because the derivative of the spline with respect to any constant
////			in the tree is 0
//			if(r <= rin || constant!=null){term1 = 0.0;}
//			else {
//				double valuechild =  m_ChildNodes[0].getValue(site.getBond(neighborNum), definingSite, axis, null);
//				double bondDerivative = neighborBond.getDerivative(constant, definingSite, axis);
//				double d1 = rout-r;
//				double d1p2 = d1*d1;
//				double d1p3 = d1p2*d1;
//				double d1p4 = d1p3*d1;
//				double d2 = rout-rin;
//				double d2p3 = d2*d2*d2;
//				double d2p4 = d2p3*d2;
//				double d2p5 = d2p4*d2;
//				double factor0 = -30.0*d1p4/d2p5;
//				double factor1 = 60.0*d1p3/d2p4;
//				double factor2 = -30.0*d1p2/d2p3;
//				double derivative0 = factor0 + factor1 + factor2;
//				double derspline = derivative0*bondDerivative;
//				term1 = derspline*valuechild;
//			}
//			returnValue += (term0 + term1);
//		}
//		return returnValue;
//	}
	
//	 //smoothing(x) = x^4 / (1 + x^4)
//	 //where x = (r-rout) / h
//	 //Source DOI: https://doi.org/10.1088/1361-651X/ab0d75, Uncertainty quantification for classical effective potentials:an extension to POTFIT
//	if(smoothing == 's') {
//		double rout = getRout();
//		double h = 0.75;
//		
//		for (int neighborNum = 0; neighborNum < site.numNeighbors(); neighborNum++) {
//			 double r = site.getBond(neighborNum).getBondLength();
//			 double s;
//			 if(r >= rout){s = 0.0;}
//			 else{
//				 double x = (r-rout) / h;
//				 double p1 = x*x*x*x;
//				 s =  p1 / (1.0 + p1);
//			 }
//
//			Bond neighborBond = site.getBond(neighborNum);
//			double derchild = m_ChildNodes[0].getDerivative(site.getBond(neighborNum), constant, definingSite, axis);
//			double term0 = derchild*s;
//			double term1;
////			If constant is not null, term1=0 because the derivative of the spline with respect to any constant
////			in the tree is 0
//			if(constant!=null){term1 = 0.0;}
//			else {
//				double valuechild =  m_ChildNodes[0].getValue(site.getBond(neighborNum), definingSite, axis, null);
//				double bondDerivative = neighborBond.getDerivative(constant, definingSite, axis);
//				
//				double hp4 = h*h*h*h;
//				double d1 = r-rout;
//				double d1p3 = d1*d1*d1;
//				double d1p4 = d1p3*d1;
//				double numerator = 4.0*hp4*d1p3;
//				double add1 = hp4 + d1p4;
//				double denominator = add1*add1;
//				
//				double derivative0 = numerator/denominator;
//				double derspline = derivative0*bondDerivative;
//				term1 = derspline*valuechild;
//			}
//			returnValue += (term0 + term1);
//		}
//		return returnValue;
//	}
	
	else if(smoothing == 'e') {
//		Cutoff from University of Virginia, MSE 4270/6270: Introduction to Atomistic Simulations, Leonid Zhigilei
		double c = 0.1;
		double rc = 5.5;
		for (int neighborNum = 0; neighborNum < site.numNeighbors(); neighborNum++) {
			double r = site.getBond(neighborNum).getBondLength();
			double difference = r-rc;
			double smoothingValue = Math.exp(c/difference);
			double value0 = m_ChildNodes[0].getDerivative(site.getBond(neighborNum), constant, definingSite, axis)*smoothingValue;
			double value1 =  m_ChildNodes[0].getValue(site.getBond(neighborNum), definingSite, axis, null);
			double bondDerivative = site.getBond(neighborNum).getDerivative(constant, definingSite, axis);
			double value2 = (-c*smoothingValue/(difference*difference))*(bondDerivative);
			returnValue += (value0 + value1*value2);
		}
		return returnValue;
	}
	else if(smoothing == 'n') {
		for (int neighborNum = 0; neighborNum < site.numNeighbors(); neighborNum++) {
			returnValue += m_ChildNodes[0].getDerivative(site.getBond(neighborNum), constant, definingSite, axis);
		}
		return returnValue;
	}
	else {
		return Double.NaN;
	}
	
}
 

 @Override
 public Node copy() {
	  Node copy = new SiteSumNode(this.getChildNode(0), this.getUseFirstSite());
		copy.setValues(this.getNumTimesSelected());
	  return copy;	   
 }

 @Override
 public String getNodeType(){
	  return "SiteSum";
 }
 @Override
 public Integer getComplexity(){
//	 The complexity of the smoothing spline is computed creating a SiteSumNode with child multiplicationNode
//	 MultiplicationNode has as children: smoothingSpline and a constantNode. 
//	 Compute the complexity of this tree with https://doi.org/10.1007/0-387-23254-0_17 (the method getComplexity())
//	 and finally subtract 3 to account for the constantNode (child of the multiplication)
	 
//	 if(m_HasDistance == null) {
//		 TreePotential tree = new TreePotential(this.deepCopy(), 5.5);
//		 m_HasDistance = tree.hasDistance(); 
//	 }
//	 if(m_HasDistance) {
		 return 18;//17 nodes of smoothing spline and 1 of siteSum
//	 }
//	 else {
//		 return 1;//This is the complexity without smoothing spline
//	 }	 
 }
 
 @Override
 public ArrayList<String> getExpression(boolean symbolic){
	  ArrayList<String> aL = new ArrayList<String>();
	  if(symbolic) {
		  aL.add(0,"(S[");
	  }
	  else {
		  if(this.getUseFirstSite()){
			  aL.add(0,"(T[");
		  }
		  else{
			  aL.add(0,"(F[");
		  }
	  }
	  aL.addAll(1,this.getChildNode(0).getExpression(symbolic));
	  aL.add(aL.size(),"])");
	  return aL;
 }
 @Override
 public Node getLeft(){
	  return m_ChildNodes[0];
 }
 @Override
 public Node getRight(){
	  return null;
 }

 @Override
 public Double getFromKnownValues(Bond bond){
//	 Checking this is faster than not checking
	  if(this.getUseFirstSite()){return this.getKnownValues().get(bond.getCentralSiteOfBond().getDefiningSite());}
	  return this.getKnownValues().get(bond.getNeighborSiteOfBond().getDefiningSite());
 }
 
 @Override
 public void putInKnownValues(Bond bond, double value){
//	 It is faster to cache the values and check for the opposite node
	  if(this.getUseFirstSite()){m_KnownValues.put(bond.getCentralSiteOfBond().getDefiningSite(),value);}
	  else{m_KnownValues.put(bond.getNeighborSiteOfBond().getDefiningSite(),value);}	
 }

 @Override
 public Node generateNode(Node child0, Node child1) {
	return new SiteSumNode(child0,GENERATOR.nextBoolean());
 }

@Override
public Double getFromKnownDerivatives(Bond bond, matsci.structure.Structure.Site definingSite, Integer axis) {
//	 Checking this is faster than not checking
	  if(this.getUseFirstSite()){return this.getKnownDerivatives().get(new InfoHash(bond.getCentralSiteOfBond().getDefiningSite(), definingSite, axis));}
	  else{return this.getKnownDerivatives().get(new InfoHash(bond.getNeighborSiteOfBond().getDefiningSite(), definingSite, axis));}
}

@Override
public void putInKnownDerivatives(Bond bond, matsci.structure.Structure.Site definingSite, Integer axis, double value) {
//	 It is faster to cache the values and check for the opposite node
	  if(this.getUseFirstSite()){m_KnownDerivatives.put(new InfoHash(bond.getCentralSiteOfBond().getDefiningSite(), definingSite, axis),value);}
	  else{m_KnownDerivatives.put(new InfoHash(bond.getNeighborSiteOfBond().getDefiningSite(), definingSite, axis),value);}
}
@Override
public String getSymbol() {
	if(this.getUseFirstSite()) {return "T";}
	else {return "F";}
}
public static double getRout() {
	return m_rout;
}
public static double getRin() {
	return m_rin;
}
public static void setR(double rin, double rout) {
	m_rout = rout;
	m_rin = rin;
}
private boolean getUseFirstSite() {
	return m_UseFirstSite;
}
//public boolean hasDistanceNode() {
//	 if(m_HasDistance == null) {
//		 TreePotential tree = new TreePotential(this.deepCopy(), 5.5);
//		 m_HasDistance = tree.hasDistance(); 
//	 }
//	 return m_HasDistance;
//}
}