/*
 * Created on Aug 10, 2016
 *
 */
package potentialModelsGP.tree;

import java.util.ArrayList;

import evaluator.NotFiniteValueException;
import evaluator.StructureNet.Bond;

public class NaturalLogNode extends Node {

  public NaturalLogNode() {
    super(new Node[0]);
  }
  
  public NaturalLogNode(Node child) {
    super (new Node[] {child});
  }

  @Override
  public double calculateValue(Bond bond, matsci.structure.Structure.Site definingSite, Integer axis, Double daxis) throws NotFiniteValueException {
    return Math.log(this.getChildNode(0).getValue(bond, definingSite, axis, daxis));
  }
  
  @Override
  public Node generateNode(Node childNode) {
    return new NaturalLogNode(childNode);
  }

  @Override
  public double calculateDerivative(Bond bond, ConstantNode variable, matsci.structure.Structure.Site definingSite, Integer axis) throws NotFiniteValueException {
    //
    // d/dx (ln(f)) = f'/f
    //
    double f = this.getChildNode(0).getValue(bond, definingSite, axis, null);
    double fPrime = this.getChildNode(0).getDerivative(bond, variable, definingSite, axis);
    return fPrime / f;
  }

  @Override
  public Node copy() {
	  Node copy = new NaturalLogNode(this.getChildNode(0));
		copy.setValues(this.getNumTimesSelected());
	    return copy;	        
  }
  @Override
  public String getNodeType(){
	  return "NaturalLog";
  }
  @Override
  public Integer getComplexity(){
	  return 1;
  }
  @Override
  public ArrayList<String> getExpression(boolean symbolic){
	  ArrayList<String> aL = new ArrayList<String>();
	  aL.add(0,"(Log[");
	  aL.addAll(1,this.getChildNode(0).getExpression(symbolic));
	  aL.add(aL.size(),"])");
	  return aL;
  }
  @Override
  public Node getLeft(){
	  return this.getChildNode(0);
  }
  @Override
  public Node getRight(){
	  return null;
  }
  @Override
  public Double getFromKnownValues(Bond bond){
	  return null;
	  }
  @Override
  public void putInKnownValues(Bond bond, double value){
	  }

@Override
public Node generateNode(Node child0, Node child1) {
	// TODO Auto-generated method stub
	return null;
}

@Override
public Double getFromKnownDerivatives(Bond bond, matsci.structure.Structure.Site definingSite, Integer axis) {
	// TODO Auto-generated method stub
	return null;
}

@Override
public void putInKnownDerivatives(Bond bond, matsci.structure.Structure.Site definingSite, Integer axis, double value) {
	// TODO Auto-generated method stub
}
@Override
public String getSymbol() {
	return "ln";
}
}
