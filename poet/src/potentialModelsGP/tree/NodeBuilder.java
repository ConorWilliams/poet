// This is not completely implemented
package potentialModelsGP.tree;

import java.util.ArrayList;

import evaluator.NotFiniteValueException;
import evaluator.StructureNet.Bond;
import potentialModelsGP.tree.neighbors.SiteSumNode;

public class NodeBuilder extends Node{
	String m_TypeToBuild = "";
	public NodeBuilder() {
		super(new Node[2]);
	}
	
	public void setTypeToBuild(String string){
		m_TypeToBuild = string;
	}
	
	public Node build(){
		if(m_TypeToBuild.contains(".")){
			m_TypeToBuild = m_TypeToBuild.replace("W", "E-");
			return new ConstantNode(Double.parseDouble(m_TypeToBuild));
		}
		else if(m_TypeToBuild.equals("r")){
			return new DistanceNode();
		}
		else if(m_TypeToBuild.equals("+")){
			NodeBuilder child0 = (NodeBuilder)this.getChildNode(0);
			NodeBuilder child1 = (NodeBuilder)this.getChildNode(1);
			return new AdditionNode(child0.build(),child1.build());
		}
		else if(m_TypeToBuild.equals("-")){
			NodeBuilder child0 = (NodeBuilder)this.getChildNode(0);
			NodeBuilder child1 = (NodeBuilder)this.getChildNode(1);
			return new SubtractionNode(child0.build(),child1.build());
		} 
		else if(m_TypeToBuild.equals("*")){
			NodeBuilder child0 = (NodeBuilder)this.getChildNode(0);
			NodeBuilder child1 = (NodeBuilder)this.getChildNode(1);
			return new MultiplicationNode(child0.build(),child1.build());
		} 
		else if(m_TypeToBuild.equals("/")){
			NodeBuilder child0 = (NodeBuilder)this.getChildNode(0);
			NodeBuilder child1 = (NodeBuilder)this.getChildNode(1);
			return new DivisionNode(child0.build(),child1.build());		
		} 
		else if(m_TypeToBuild.equals("^")){
			NodeBuilder child0 = (NodeBuilder)this.getChildNode(0);
			NodeBuilder child1 = (NodeBuilder)this.getChildNode(1);
			return new PowerNode(child0.build(),child1.build());		
		}
		else if(m_TypeToBuild.equals("T")){
			NodeBuilder child1 = (NodeBuilder)this.getChildNode(1);
			return new SiteSumNode(child1.build(),true);		
		}
		else if(m_TypeToBuild.equals("F")){
			NodeBuilder child1 = (NodeBuilder)this.getChildNode(1);
			return new SiteSumNode(child1.build(),false);		
		}
		else{
			return null;
		}
	}
	
	@Override
	public Node generateNode(Node childNode) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Node generateNode(Node child0, Node child1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void putInKnownValues(Bond bond, double value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Double getFromKnownValues(Bond bond) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public double calculateValue(Bond bond, matsci.structure.Structure.Site definingSite, Integer axis, Double daxis) throws NotFiniteValueException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double calculateDerivative(Bond bond, ConstantNode variable, matsci.structure.Structure.Site definingSite, Integer axis) throws NotFiniteValueException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Node copy() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getNodeType() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getComplexity() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<String> getExpression(boolean symbolic) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Node getLeft() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Node getRight() {
		
		return null;
	}

	@Override
	public Double getFromKnownDerivatives(Bond bond, matsci.structure.Structure.Site definingSite, Integer axis) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void putInKnownDerivatives(Bond bond, matsci.structure.Structure.Site definingSite, Integer axis, double value) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public String getSymbol() {
		return "nodeBuilder";
	}

}
