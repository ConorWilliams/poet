package evaluator;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Random;
import java.util.Stack;


import gAEngine.IIndividual;
import gAEngine.InfoAddIndividual;
import matsci.io.app.log.Status;
import util.Statistics;

public class PotentialSubPopulation {
	
	public static final Random GENERATOR = new Random();
	private ArrayList<IIndividual> m_Individuals;
//    private double m_SumSelectivityScore;
    private int m_PopulationNumber;
//    public int m_SelectionCumulativeWeight;
    private String m_Type;
	
public PotentialSubPopulation(ArrayList<IIndividual> individuals, int populationNumber,String type){
	m_Individuals = new ArrayList<IIndividual>(individuals);
	m_PopulationNumber = populationNumber;
	m_Type = type;
	
}
	
public ArrayList<IIndividual> getIndividuals() {
	return m_Individuals;
}
		
//private synchronized InfoAddIndividual addIndividualPareto(IIndividual prospectiveIndividual, boolean training, Double alpha, boolean fitnessByLC){
//
//	PotentialEvaluator prospective = (PotentialEvaluator)prospectiveIndividual;
//	ArrayList<IIndividual> deleteIndividuals = new ArrayList<IIndividual>();
//	InfoAddIndividual returnObject = new InfoAddIndividual(false, deleteIndividuals, this.getPopulationNumber());
//	
//	if(prospective.getFitness(true, null,fitnessByLC) <= m_ConvergenceThreshold){returnObject.setConverged(true);}
//		
//	//		fp and cp are the fitness and complexity of prospective individual
//	double fp = prospective.getFitness(training, alpha,fitnessByLC);
//	int cp = prospective.getComplexity();
//	
//	//		Get individual with greatest complexity but less than that of prospectiveIndividual. 
//	int iB = this.getIndexLessComplexityPareto(prospective);
//			
//	
//	//If returns null, then should not add and should not remove any individual
//	//The first index of the returned array: 
//	//	If Integer.MaxValue() then it should be appended
//	//	If it is any other integer, then prospective should be added at that position
//	//The second index of the returned array
//	//	If it is 0, then no individual must be removed from the population
//	//	If it is 1, then we have to analyze whether or not we must delete individuals from the population
//	Integer[] info = this.getInfoToAddToPareto(prospective, fp, cp, iB, training, alpha, fitnessByLC);
//	
//	if(info == null) {return returnObject;}
//	
//	else {
//		if(info[1].equals(1)) {
//			deleteIndividuals = this.getIndividualsToDeletePareto(fp, iB, training, alpha, fitnessByLC);
//			m_Individuals.removeAll(deleteIndividuals);
//		}
//		if(info[0].equals(Integer.MAX_VALUE)) {m_Individuals.add(prospective);}
//		else {m_Individuals.add(info[0],prospective);}
//		
//		returnObject.setIndividuals(deleteIndividuals);
//		returnObject.setAdded(true);
//		
//		return returnObject;
//	}
//}

//private ArrayList<IIndividual> getIndividualsToDeletePareto(double fp, int iB, boolean training, Double alpha, Boolean fitnessByLC){
////	m_Individuals follows the Pareto non-dominance criterion (i.e., it is a Pareto frontier)
////	m_Individuals is sorted so that the least complex is at index 0. 
////	m_Individuals is also sorted with respect to fitness
////	The worst fitness corresponds to the minimum complexity
////	index iB corresponds to individual with greatest complexity but less than that of prospectiveIndividual. 
////	fp is the fitness of prospectiveIndividual
//
//	ArrayList<IIndividual> deleteThese = new ArrayList<IIndividual>();
//	
//	int index = 0;
//	for(IIndividual indiv : m_Individuals){	
////			Do not analyze less complex individuals
//			if(index > iB){	
//				
////			If the size doesn't change, then we can break this loop
//			int initialSize = deleteThese.size();
//			
//			double findiv = indiv.getFitness(training, alpha,fitnessByLC);
//			if(fp < findiv) {
//				deleteThese.add(indiv);
//			}
//			
////			If the size doesn't change, then all the next individuals are more fit than prospective 
////			or they have the same fitness as prospective, so we can break this forLoop
//			if(initialSize == deleteThese.size()){break;};
//		}
//		index++;
//	}	
//	return deleteThese;
//}


//private Integer[] getIndexToAddToPareto(PotentialEvaluator prospective, double fp, int cp, int iB, boolean training, Double alpha, Boolean fitnessByLC) {
////	iB = index of the individual in the population with the greatest complexity less than the complexity of prospective
////	fiB = fitness of individual at index iB
////	fp = fitness of prospective individual
//		
//	
////	When there is an individual with the same types of nodes, only add if fp < fequalNodes
////	Find the individuals with the same complexity, because they are the ones that might have the same types of nodes
//	ArrayList<IIndividual> individualsWithSameComplexity = this.getIndividualsWithSameComplexityPareto(cp, iB);
//	for (IIndividual indivSameComplx : individualsWithSameComplexity) {
//		PotentialEvaluator evaluatorSameComplx = (PotentialEvaluator)indivSameComplx;
//		String typesIndivPop = evaluatorSameComplx.getNodeTypes();
//		String typesProsp = prospective.getNodeTypes();
//		boolean hasSameTypes = typesIndivPop.equals(typesProsp);
//		
//		double individualInPopFitness = evaluatorSameComplx.getFitness(training, alpha,fitnessByLC);
//		
//		boolean prospectiveMoreFit =  fp < individualInPopFitness;
//
//		if(hasSameTypes && !prospectiveMoreFit){
//			return null;
//		}
//	}
//
////	Case1
//	if(iB == -1) {
//		double fiB = m_Individuals.get(0).getFitness(training, alpha,fitnessByLC);
//		if(fp <= fiB) {
//			return new Integer[] {0,0};
//		}
//		else{return null;}
//	}
//	double fiB = m_Individuals.get(iB).getFitness(training, alpha,fitnessByLC);
//	if(fp < fiB) {
////		Case3
//		boolean moreComplexThanMostComplex = (iB == (m_Individuals.size() - 1));
//		if(moreComplexThanMostComplex) {
//			return new Integer[] {Integer.MAX_VALUE,0};
//		}
////		Case5
//		else {
//			IIndividual individualiBplusOne = m_Individuals.get(iB+1);
//			double fiBplusOne = individualiBplusOne.getFitness(training, alpha, fitnessByLC);
//			int ciBplusOne = individualiBplusOne.getComplexity();
//			boolean dontAdd = fp > fiBplusOne && cp == ciBplusOne;
//			if(dontAdd){return null;}
//			else {return new Integer[] {iB+1,0};}
//		}
//	}
//	else {return null;}
//}


//// Get individuals with a complexity equal to "complexity", these individuals should also have the same fitness, 
//// because they are in the Pareto frontier
//// iB is the index of the individual with the greatest complexity that is < "complexity"
//private ArrayList<IIndividual> getIndividualsWithSameComplexityPareto(int complexity, int iB) {
//	ArrayList<IIndividual> sameComplexity = new ArrayList<IIndividual>();
//	int c = iB;
//	int size = m_Individuals.size();
//	while(c + 1 < size) {
//		IIndividual individual = m_Individuals.get(c+1);
//		if(individual.getComplexity() == complexity) {sameComplexity.add(individual);}
////		If we are analyzing individuals with greater complexity, then it means that there will be no more individuals with the same complexity
//		else if(individual.getComplexity() > complexity) {return sameComplexity;}
//		c++;
//		
//	}
//	return sameComplexity;
//}


////If returns null, then should not add and should not remove any individual
////The first index of the returned array: 
////	If Integer.MaxValue() then it should be appended
////	If it is any other integer, then prospective should be added at that position
////The second index of the returned array
////If it is 0, then no individual must be removed from the population
////If it is 1, then we have to analyze whether or not we must delete individuals from the population
//private Integer[] getInfoToAddToPareto(PotentialEvaluator prospective, double fp, int cp, int iB, boolean training, Double alpha, Boolean fitnessByLC) {
//	
//	boolean iBisMinusOne = (iB == -1);
//	boolean iBIsLast = (iB == m_Individuals.size() - 1);
//	if(!iBisMinusOne) {
//		double fiB = m_Individuals.get(iB).getFitness(training, alpha, fitnessByLC);
////		Cases 1 and 2
//		if(fp >= fiB) {return null;}
////		Case 3
//		if(iBIsLast){return new Integer[] {Integer.MAX_VALUE,0};}
//	}
//	if(iBisMinusOne) {
//		double f0 = m_Individuals.get(0).getFitness(training, alpha, fitnessByLC);
//		int c0 = m_Individuals.get(0).getComplexity();
////		Case 4
//		if(fp < f0 && cp <= c0) {return new Integer[] {0,1};}
////		Case 6
//		if(fp > f0 && cp < c0) {return new Integer[] {0,0};}
////		Case 7
//		if(fp > f0 && cp == c0) {return null;}
////		Case 5
//		if(fp == f0 && cp < c0) {m_Individuals.remove(0); return new Integer[] {0,0};}
//		String typesP = prospective.getNodeTypes();
//		PotentialEvaluator eval0 = (PotentialEvaluator)m_Individuals.get(0);
//		String types0 = eval0.getNodeTypes();
//		boolean sameTypes = typesP.equals(types0);
////		Case 11 
//		if(fp == f0 && cp == c0 && sameTypes) {return null;}
////		Case 12
//		if(fp == f0 && cp == c0 && !sameTypes) {return new Integer[] {0,0};}
//
//	}
//	
//	if(!iBisMinusOne && !iBIsLast) {
//		double fiB = m_Individuals.get(iB).getFitness(training, alpha, fitnessByLC);
//		if(fp < fiB) {
//			int ciB = m_Individuals.get(iB).getComplexity();
//			int ciBplusOne = m_Individuals.get(iB+1).getComplexity();
////			Case 8
//			if(ciB < cp && cp < ciBplusOne) {return new Integer[] {iB+1,1};}
//			double fiBplusOne = m_Individuals.get(iB+1).getFitness(training, alpha, fitnessByLC);
////			Case 9
//			if(cp == ciBplusOne && fp > fiBplusOne) {return null;}
////			Case 10
//			if(cp == ciBplusOne && fp < fiBplusOne) {return new Integer[] {iB+1,1};}
//			if(cp == ciBplusOne && fp == fiBplusOne) {
//				Boolean addIndWithSameComplexity = null;
//				ArrayList<IIndividual> individualsWithSameComplexity = this.getIndividualsWithSameComplexityPareto(cp, iB);
//				for (IIndividual indivSameComplx : individualsWithSameComplexity) {
//					PotentialEvaluator evaluatorSameComplx = (PotentialEvaluator)indivSameComplx;
//					int complxSameComplx = evaluatorSameComplx.getComplexity();
//					String typesIndivPop = evaluatorSameComplx.getNodeTypes();
//					String typesProsp = prospective.getNodeTypes();
//					boolean sameTypes = typesIndivPop.equals(typesProsp);
//					double fitIndivSameComplx = evaluatorSameComplx.getFitness(training, alpha,fitnessByLC);
//					if(cp == complxSameComplx && fp == fitIndivSameComplx) {//Just to be completely sure
////						Case 13, part 1
//						if(!sameTypes) {
//							addIndWithSameComplexity = true;
//						}
////						Case 14
//						else {return null;}
//					}
//				}
//				if(addIndWithSameComplexity == null) {
//					Status.basic("Problem with cases 13 and 14! Class PotentialSubPopulation");
//					System.exit(0);
//				}
////				Case 13, part2
//				if(addIndWithSameComplexity) {return new Integer[] {iB+1,0};}
//			}
//		}
//	}
//	Status.basic("Problem in Class PotentialSubPopulation: it is going until the last return statement");
//	System.exit(0);
//	return null;
//}


	
////	Get the index of an individual in the population that has a complexity less than the complexity of prospective
////  complexity of individual at returned integer < complexity prospective
//	private int getIndexLessComplexityPareto(IIndividual prospective){
////		Assume that m_Individuals is sorted so that the smallest is at index 0
////		Get individual with greatest complexity less than that of prospectiveIndividual
//		double prospectiveComplexity = prospective.getComplexity();
//		int index = 0;
//		for(IIndividual indiv : m_Individuals){
//			double indivComplexity = indiv.getComplexity();
//			if(prospectiveComplexity <= indivComplexity){
//				break;
//			}
//			index++;
//		}
//		return index - 1 ;
//	}
	
//	private void updateCumulativeSelectivityScores(){
//		double cumulativeSelectivity = 0;
//		int count = 0;
//		for(IIndividual individual : m_Individuals){
//				if(count==0){
//					individual.setCumulativeSelectivityScore(0);
//				}
//				else{
//					cumulativeSelectivity += individual.getSelectivityScore();
//					individual.setCumulativeSelectivityScore(cumulativeSelectivity);
//				}
//				count++;
//		}
//		m_SumSelectivityScore = cumulativeSelectivity;
//	}
	
//	public boolean converged() {
//		for(IIndividual individual : m_Individuals){
//			if( individual.getFitness(true, null) <= m_ConvergenceThreshold ){return true;}
//		}
//		return false;
//	}
	

//private IIndividual getIndividualEqualProbability() {
//	if(m_Type.equals("BinsHFC")|| m_Type.equals("HFC")){
//		int size = this.getIndividuals().size();
//		if(size == 0){return null;}
//		int index = (int)(GENERATOR.nextDouble() * size);
//		return this.getIndividuals().get(index);
//	}
//	else{
//		return null;
//	}
//}
	
//private IIndividual getIndividualSelectivity() {
////		Return the individual with the least cumulative weight greater than or equal to value
////	    For example: we add iron, stone and diamond with weights 2, 3 and 5 respectively; then 
////	    the cumulative weights will be 2, 5 and 10 respectively. 
////	    If we choose a random number
////	    between 0 and 10, and use this number to loop checking: value <= individual.getCumulativeWeight(), 
////		then the probability of selecting stone (with weight of 3 but with key of 5) will be the
////	    probability of choosing a random number (minimum 0 and max 10) between 2 and 5, which is 0.3. 
//	
////		size = 1 when only the tree that only has one node (which is constant) is in the population
//	if(m_Individuals.size() == 1){return null;}
//	this.updateCumulativeSelectivityScores();
//	double value;
//	do{
//		value = GENERATOR.nextDouble()*m_SumSelectivityScore;
//	}while(value == 0);
//	for(IIndividual individual : m_Individuals){
//		if(value < individual.getCumulativeSelectivityScore()){
//			return individual;
//		}
//	}
//	return null;
//}
	
public void print(boolean usePath, String path, String population, int num) {
	m_Individuals = PotentialSubPopulation.sortIndividualsParetoOrLowerConvexHull(this.getIndividuals());
	if(usePath){
		try{
		    FileWriter fw = new FileWriter(path+"/"+population);
			BufferedWriter bw = new BufferedWriter(fw);
			PrintWriter out = new PrintWriter(bw);
			
			for (IIndividual individual : this.getIndividuals()) {
				PotentialEvaluator evaluator = (PotentialEvaluator) individual;
				out.println(evaluator.getFitness(null, true)+" "+evaluator.getFitness(null, false)+" "+evaluator.getComplexity()+" "+evaluator.numNodes()+" "+evaluator.getExpression());
			}
			out.close();
			return;
		}catch(IOException e){
			Status.basic("Problem printing from subpopulation!");
			e.printStackTrace();
			return;
		}
	}
	else{
		Status.basic(population+" "+(num)+" "+m_Individuals.size());
		for (IIndividual individual : this.getIndividuals()) {
			PotentialEvaluator evaluator = (PotentialEvaluator) individual;
			Status.basic(evaluator.getFitness(null, true)+" "+evaluator.getFitness(null, false)+" "+evaluator.getComplexity()+" "+evaluator.numNodes()+" "+evaluator.getExpression());
		}
		return;
	}
}
	
public int getPopulationNumber(){
	return m_PopulationNumber;
}
	
//	private InfoAddIndividual addBinsHFC(IIndividual prospective, int numPops){
//		InfoAddIndividual info = new InfoAddIndividual(false, new ArrayList<IIndividual>(), m_PopulationNumber);
//		double fitness = prospective.getFitness(true, null);
//		if(fitness <= 1E-12){
//			info.setConverged(true);
//		}
//		double threshold = (((double)m_PopulationNumber)/(numPops-1)) + (1/numPops);
//		if(fitness < threshold){
//			m_Individuals.add(prospective);
//			info.setAdded(true);
//			return info;
//		}
//		return info;
//	}
//	
	public void setIndividuals(ArrayList<IIndividual> individuals) {
		m_Individuals = new ArrayList<IIndividual>(individuals);
	}

public IIndividual getIndividual(String selectionMethod) {
	if(m_Type.equals("List")) {
		if(selectionMethod.equals("Equal")){
			return this.getIndividuals().get(GENERATOR.nextInt(this.getIndividuals().size()));
		}
	}
	else if(m_Type.equals("ParetoFrontier")){
//			if(selectionMethod.equals("SelectivityScore")){
//				return this.getIndividualSelectivity();
//			}
		if(selectionMethod.equals("Equal")){
			return this.getIndividuals().get(GENERATOR.nextInt(this.getIndividuals().size()));
		}
	}
	else if(m_Type.equals("ConvexHull")){
		if(selectionMethod.equals("Equal")){
			return this.getIndividuals().get(GENERATOR.nextInt(this.getIndividuals().size()));
		}	
	}
//	else if(m_Type.equals("BinsHFC")){
////		if(selectionMethod.equals("SelectivityScore")){
////			return this.getIndividualSelectivity();
////		}
//		else if(selectionMethod.equals("Equal")){
//			return this.getIndividualEqualProbability();
//		}
//	}
//	else if(selectionMethod.equals("HFC")){
//		return this.getIndividualEqualProbability();
//	}
	return null;
}

public InfoAddIndividual add(IIndividual prospective, Double alpha, Integer numPops, boolean fitnessByLC){
	if(m_Type.equals("List")) {
		this.getIndividuals().add(prospective);
		InfoAddIndividual info = new InfoAddIndividual(true, null, this.getPopulationNumber());
		if(Statistics.compare(prospective.getFitness(null, false), PotentialEvaluator.m_Convergence) == -1){
			info.setConverged(true);
		}
		else if(Statistics.compare(prospective.getFitness(null, true), PotentialEvaluator.m_Convergence) == -1){
			info.setConverged(true);
		}
		return info;
	}
	else if(m_Type.equals("ConvexHull")){
		//If an individual is not added to the Pareto frontier, then it should not be added to the
		//convex hull. In other words, the individuals in the convex hull must be in the Pareto frontier
		InfoAddIndividual info = this.addToParetoFrontier(prospective, alpha, fitnessByLC);
		if(info.added()) {
			//Convert the Pareto frontier into the convex hull
			m_Individuals = PotentialSubPopulation.getLowerConvexHullFromParetoFrontier(m_Individuals, fitnessByLC);
			//Check if prospective is in the convex hull 
			info.setAdded(m_Individuals.contains(prospective));
		}
		return info;
	}
	else if(m_Type.equals("ParetoFrontier")){
//		boolean hasEvolutionPath = ((PotentialEvaluator)prospective).getEvolutionPath() != null;
		
//		ArrayList<IIndividual> prevInds = new ArrayList<IIndividual>();
//		for (IIndividual indiv : this.getIndividuals()) {
//			prevInds.add(indiv);
//		}
		
		InfoAddIndividual info = this.addToParetoFrontier(prospective, alpha, fitnessByLC);
//		if(!fitnessByLC && hasEvolutionPath && info.added()) {
//			this.sortIndividualsPareto();
//			int m = 0,n = 0;
//			Double change = null;
//			ArrayList<IIndividual> individualsSameComplx = this.getIndividualsWithComplexity(prospective.getComplexity());
//
//			if(individualsSameComplx.size() > 1) {
//				//The improvement in fitness is zero if the individual added has the same
//				//complexity as any other individual in the current population
//				change = 0.0;
//			}
//
//			else if(prospective.equals(this.getIndividuals().get(0))) {
//				//Only interested in improvements in fitness
//				//Added Prospective as the least complex
//				//Compare Prospective to the previous least complex individual
//				//Prospective could be better or worse than the least complex individual in the previous population
//				IIndividual im = prospective;
//				IIndividual in = PotentialSubPopulation.getLeastFit(prevInds, fitnessByLC, true);
//				double fm = im.getFitness(alpha, fitnessByLC);
//				double fn = in.getFitness(alpha, fitnessByLC);
//				if(fm < fn) {
//					//If Prospective is better
//					change = (fn - fm)/(fn);}
//				else{
//					change = 0.0;}
//			}
//			else {
//				//get the index of individual
//				int index = 0;
//				for (IIndividual indiv : this.getIndividuals()) {
//					if(indiv.equals(prospective)) {break;}
//					index++;
//				}
//				m = index - 1;
//				n = index;
//			}
//			if(change == null) {
//				IIndividual im = this.getIndividuals().get(m);
//				IIndividual in = this.getIndividuals().get(n);
//				double fm = im.getFitness(alpha, fitnessByLC);
//				double fn = in.getFitness(alpha, fitnessByLC);
//				change = (fm - fn)/(fm);
//			}
//			((PotentialEvaluator)prospective).updateAvgScore(change);
//		}
//		else if(!fitnessByLC && hasEvolutionPath && !info.added()) {
//			((PotentialEvaluator)prospective).updateAvgScore(0.0);
//		}
		return info;

	}
//	else if(m_Type.equals("HFC")) {
//		return this.addBinsHFC(prospective,numPops);
//	}
	else {
		return null;
	}
}

protected Double getAreaUnderParetoFrontier(boolean training, Double alpha, boolean fitnessByLC) {
//	Only analyze if size is greater than 1 because area is zero otherwise, although it could be zero even if the size is greater than 1
	if(!(1 < this.getIndividuals().size())) {return 0.0;}
	double area = 0.0;
	//fnmins1 = fitness of individual at (n-1)
	double fn = this.getIndividuals().get(0).getFitness(alpha, fitnessByLC);
	int cn = this.getIndividuals().get(0).getComplexity();
	double fm; 
	int cm;
	for (IIndividual indivn : this.getIndividuals()) {
		fm = indivn.getFitness(alpha, fitnessByLC);
		cm = indivn.getComplexity();
		//Area of trapezoid
		double f = 0.5*(fn + fm);
		double diffc = cm - cn;
		area += diffc*f;
		fn = fm;
		cn = cm;
	}
	return area;
}

private InfoAddIndividual addToParetoFrontier(IIndividual prospective, Double alpha, boolean fitnessByLC){
	ArrayList<IIndividual> dominatedByProsp = new ArrayList<IIndividual>();
	InfoAddIndividual info = new InfoAddIndividual(false, dominatedByProsp, this.getPopulationNumber());
	
	double fp = prospective.getFitness(alpha, fitnessByLC);
	int cp = prospective.getComplexity();
	
	for (IIndividual individual : m_Individuals) {
		double fi = individual.getFitness(alpha, fitnessByLC);
		int ci = individual.getComplexity();
//		If prospective is dominated, then return
		if(ci <= cp && fi < fp) {
			return info;
		}
//		If prospective is dominated, then return
		if(ci < cp && fi <= fp) {
			return info;
		}
//		If prospective is equal to another individual in the population, then return
		if(ci == cp && Statistics.compare(fi, fp)==0) {
			ArrayList<IIndividual> sameComplx = this.getIndividualsWithComplexity(cp);
			String prospTypes = ((PotentialEvaluator)prospective).getNodeTypes();
			for (IIndividual indiv : sameComplx) {
				String indivTypes = ((PotentialEvaluator)indiv).getNodeTypes();
				if(prospTypes.equals(indivTypes)) {return info;}
			}
		}
		
//		If individual is dominated by prospective, then add individual to dominatedByProsp
		if(cp <= ci && fp < fi) {
			dominatedByProsp.add(individual);
		}
		else if(cp < ci && fp <= fi) {
			dominatedByProsp.add(individual);
		}
	}
	
	m_Individuals.removeAll(dominatedByProsp);
	m_Individuals.add(prospective);
	if(Statistics.compare(prospective.getFitness(alpha, fitnessByLC), PotentialEvaluator.m_Convergence) == -1){
		info.setConverged(true);
	}
	info.setAdded(true);
	info.setIndividuals(dominatedByProsp);
	return info;
}

private ArrayList<IIndividual> getIndividualsWithComplexity(int complexity){
	ArrayList<IIndividual> individuals = new ArrayList<IIndividual>();
	for (IIndividual individual : m_Individuals) {
		if(individual.getComplexity() == complexity) {
			individuals.add(individual);
		}
	}
	return individuals;
}


protected static ArrayList<IIndividual> sortIndividualsParetoOrLowerConvexHull(ArrayList<IIndividual> individuals) {
	ArrayList<IIndividual> temp =new ArrayList<IIndividual>();
	for (IIndividual individual : individuals) {
		temp.add(individual);
	}
	ArrayList<IIndividual> sorted = new ArrayList<IIndividual>();

	while(temp.size() > 0) {
		IIndividual individual = PotentialSubPopulation.getLeastComplex(temp);
		sorted.add(individual);
		temp.remove(individual);
	}
	return sorted;
}

private static IIndividual getLeastComplex(ArrayList<IIndividual> individuals) {
	IIndividual minComplexityIndiv = null;
	int minComplexity = Integer.MAX_VALUE;
	for (IIndividual individual : individuals) {
		if(individual.getComplexity() < minComplexity) {
			minComplexityIndiv = individual;
			minComplexity = minComplexityIndiv.getComplexity();
		}
	}
	return minComplexityIndiv;
}
protected IIndividual getFittest(ArrayList<IIndividual> individuals, boolean fitnessByLC, boolean sort) {
	int size = individuals.size();
	if( size == 0) {return null;}
	IIndividual fittest = individuals.get(0);
	if(size == 1) {return fittest;}
	
	double bestFitness = fittest.getFitness(null, fitnessByLC);
	for (IIndividual indiv : individuals) {
		if(indiv.getFitness(null, fitnessByLC) < bestFitness) {
			fittest = indiv;
			bestFitness = indiv.getFitness(null, fitnessByLC);
		}
	}
	return fittest;
}
protected static IIndividual getLeastFit(ArrayList<IIndividual> individuals, boolean fitnessByLC, boolean sort) {
	int size = individuals.size();
	if( size == 0) {return null;}
	IIndividual leastFit = individuals.get(0);
	if(size == 1) {return leastFit;}
	
	double worstFitness = leastFit.getFitness(null, fitnessByLC);
	for (IIndividual indiv : individuals) {
		if(indiv.getFitness(null, fitnessByLC) > worstFitness) {
			leastFit = indiv;
			worstFitness = indiv.getFitness(null, fitnessByLC);
		}
	}
	return leastFit;
}

public static ArrayList<IIndividual> getParetoFrontier(ArrayList<IIndividual> individuals, boolean fitnessByLC){
	ArrayList<IIndividual> paretoFrontier = new ArrayList<IIndividual>();
	paretoFrontier.add(individuals.get(0));
	PotentialSubPopulation population = new PotentialSubPopulation(paretoFrontier, 0, "ParetoFrontier");
	for (IIndividual indiv : individuals) {
		population.add(indiv, null, null, fitnessByLC);
	}
	return population.getIndividuals();
}

/**
 * Builds the convex hull of a Pareto Frontier. 
 * This method sorts the frontier from in ascending complexity
 * @param paretoFrontier
 * @param fitnessByLC
 * @return convex hull
 */
protected static ArrayList<IIndividual> getLowerConvexHullFromParetoFrontier(ArrayList<IIndividual> paretoFrontier, boolean fitnessByLC){
	ArrayList<IIndividual> frontier = PotentialSubPopulation.sortIndividualsParetoOrLowerConvexHull(paretoFrontier);

	if(frontier.size() < 3) {return frontier;}
	Stack<IIndividual> hull = new Stack<IIndividual>();
	IIndividual first = frontier.get(0);
	
	hull.push(first);
	hull.push(frontier.get(1));
	
	int c = 0;
	
	for (IIndividual indiv : frontier) {		
		if(c > 1) {
			//counter-clockwise rotation or collinear
			boolean ccw;
			do {
				IIndividual top = hull.pop();
				if(top.equals(first)) {
					ccw = true;
				}
				else {
					ccw = PotentialSubPopulation.ccw(hull.peek(), top, indiv,fitnessByLC) <= 0;
				}
				if(ccw) {
					hull.push(top);
					hull.push(indiv);
				}
			}while(!ccw);
		}
		c++;
	}
    
    ArrayList<IIndividual> hullList = new ArrayList<IIndividual>();
    for (IIndividual indiv : hull) {hullList.add(indiv);}
    
	return hullList;
}

/**
 * Determine sense of rotation from going from a to b to c
 * 
 * @param a
 * @param b
 * @param c
 * @param fitnessByLC
 * @return -1 if clockwise, 1 if counter-clockwise, 0 if collinear
 */
private static int ccw(IIndividual a, IIndividual b, IIndividual c, boolean fitnessByLC) {
	int ax = a.getComplexity();
	int bx = b.getComplexity();
	int cx = c.getComplexity();
	double ay = a.getFitness(null, fitnessByLC);
	double by = b.getFitness(null, fitnessByLC);
	double cy = c.getFitness(null, fitnessByLC);
	
	double area = (bx-ax)*(cy-by)-(cx-bx)*(by-ay);
	if(area>0) {return -1;}		//counter clockwise
	else if(area<0) {return 1;} //clockwise
	else {return 0;}			//collinear

}

protected static double distance(IIndividual a, IIndividual b, boolean fitnessByLC) {
	int ax = a.getComplexity();
	int bx = b.getComplexity();
	double ay = a.getFitness(null, fitnessByLC);
	double by = b.getFitness(null, fitnessByLC);

	double diffxSq = Math.pow(ax - bx, 2.0);
	double diffySq = Math.pow(ay - by, 2.0);
	return Math.pow(diffxSq+diffySq, 0.5);
}

/**
 * The distance to the Convex Hull has the same units of the fitness
 * 
 * @param individual
 * @param fitnessByLC
 * @param convexHull
 * @param sort
 * @return distance to convex hull
 */
protected static double distanceToConvexHull(IIndividual individual, boolean fitnessByLC, ArrayList<IIndividual> convexHull, boolean sort) {
	if(sort) {convexHull = sortIndividualsParetoOrLowerConvexHull(convexHull);}
	double fitnessIndividual = individual.getFitness(null, fitnessByLC);
	int complxIndividual = individual.getComplexity();
	IIndividual neighborLessOrEqualComplexity = null;
	IIndividual neighborMoreComplexity = null;
	for (IIndividual indiv : convexHull) {
		int cindiv = indiv.getComplexity();
		if(cindiv <= complxIndividual && neighborLessOrEqualComplexity == null) {
			if(cindiv == complxIndividual) {
				return individual.getFitness(null, fitnessByLC)-indiv.getFitness(null, fitnessByLC);
			}
			neighborLessOrEqualComplexity = indiv;
		}
		if(cindiv > complxIndividual) {
			neighborMoreComplexity = indiv;
			break;
		}
	}
//	TODO consider cases: least complex and most complex
//	if(neighborLessOrEqualComplexity==null) {
//		return Double.MAX_VALUE;
//	}
	double slope = (neighborMoreComplexity.getFitness(null, fitnessByLC)-neighborLessOrEqualComplexity.getFitness(null, fitnessByLC))/(neighborMoreComplexity.getComplexity()-neighborLessOrEqualComplexity.getComplexity());
	double intercept = neighborMoreComplexity.getFitness(null, fitnessByLC)-slope*neighborMoreComplexity.getComplexity();
	double fitnessHull = individual.getComplexity()*slope + intercept;
	double distance = fitnessIndividual - fitnessHull;
	return distance;
}
}
