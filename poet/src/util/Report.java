package util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import evaluator.DataSet;
import evaluator.Main;
import evaluator.PotentialEvaluator;
import matsci.io.app.log.Status;
import potentialModelsGP.tree.TreePotential;

public class Report {
	
	private PotentialEvaluator[] m_Evaluators;
	private DataSet m_DataSet;
	private String m_PopulationFile;

	public Report(DataSet dataSet, String populationFile) {
		m_DataSet = dataSet;
		m_PopulationFile = populationFile;
		
	}
	
	private ArrayList<ArrayList<String>> printPredictedVsTarget_MD(String directory, String errorMetric) {

		this.getDataSet().setSubset("All");
		
		File directoryFile = new File(directory);
		directoryFile.mkdir();

		//Print the error of every evaluator with respect to every property in the data set
		String[] targetProperties = this.getDataSet().getTargetProperties();
		PotentialEvaluator[] evaluators = this.getEvaluators();
		Arrays.sort(targetProperties);
		//Array for a file of models (lines) and errors (columns)
		ArrayList<ArrayList<String>> errorsallmodels = new ArrayList<ArrayList<String>>();
		for (int i = 0; i < evaluators.length + 1; i++) {
			errorsallmodels.add(new ArrayList<String>());
		}
		
		errorsallmodels.get(0).add("fitness,weighted_sum_of_normalized_MSEs,unitless,not_applicable,not_applicable");
		for (int i = 0; i < evaluators.length; i++) {
			double fitness = 0.0;
			if(this.getDataSet().getTitle().equals("Training")) {
				fitness = evaluators[i].getFitness(null, false);
			}
			else if(this.getDataSet().getTitle().equals("Validation")) {
//				String[] properties = new String[]{"Energy", "Force", "Stress"}; // TODO how to use data of a different name than the one used for training for validation
				String[] properties = this.getDataSet().getTargetProperties();
				for (String string : properties) {
					if(!(string.equals("Energy")||string.equals("Force")||string.equals("Stress"))) {// TODO check this
						continue;
					}
					double[] predicted = evaluators[i].getPredictedProperty(string);
					double[] target = evaluators[i].getDataSet().getTargetProperty(string);
					fitness += evaluators[i].getDataSet().getWeight(string)*evaluators[i].getFitness(target, predicted, false, string == "Energy");
				}
			}
			else {
				Status.basic("Fitness for the dataset "+this.getDataSet().getTitle()+" is not available!");
				System.exit(0);
			}
			errorsallmodels.get(i+1).add(Double.toString(fitness));
		}

		for (String property : targetProperties) {
			boolean property_0k = !(property.equals("Energy") || property.equals("Force") || property.equals("Stress"));
			if(property_0k) {
				continue;
			}
			
			double[] target = this.getDataSet().getTargetProperty(property);
			double sdTarget = Statistics.getStandardDeviation(target, true);
			String unitsStr = "";
			if(property.equals("Energy")) {
				unitsStr = "eV/atom";
			}
			else if(property.equals("Force")) {
				unitsStr = "eV/Angstrom";
			}
			else if(property.equals("Stress")) {
				unitsStr = "GPa";
			}
			else {
				Status.basic("No units available for "+property+" !");
			}
			errorsallmodels.get(0).add(property+","+errorMetric+","+unitsStr+","+sdTarget+","+unitsStr);
			
			for (int i = 0; i < evaluators.length; i++) {
				double[] predicted = evaluators[i].getPredictedProperty(property);
				
				double pcc = Statistics.getPCC(target, predicted);
				
				if(property.equals("Energy")) {
					double minTarget = Statistics.getMinValue(target);
					double minPredicted = Statistics.getMinValue(predicted);
					target = Statistics.arrayAdd(target, -minTarget);
					predicted = Statistics.arrayAdd(predicted, -minPredicted);
				}
				double error = Double.NaN;
				if(errorMetric.equals("MAE")) {
					error = Statistics.getMAE(target, predicted);
				}
				else if(errorMetric.equals("RMSE")) {
					error = Statistics.getRMSE(target, predicted);
				}
				else {
					new Exception().printStackTrace();
					Status.basic("The error metric "+errorMetric+" is not available!");
				}
				errorsallmodels.get(i+1).add(Double.toString(error));
				
				PrintWriter outData = null;
				PrintWriter outErrorMetrics = null;
				try{			
					outErrorMetrics = new PrintWriter(new BufferedWriter(new FileWriter(directory+"/"+(i+1)+"errorMetrics"+this.getDataSet().getTitle()+".data",true)));
					outData = new PrintWriter(new BufferedWriter(new FileWriter(directory+"/"+(i+1)+property+this.getDataSet().getTitle()+".data")));
				}catch(IOException e){
					Status.basic("Error writing!");
					e.printStackTrace();
				}
				outErrorMetrics.println(this.getDataSet().getDataDirectoryName()+" "+this.getDataSet().getTitle()+" "+property+" "+errorMetric+" "+error+" "+sdTarget+" "+pcc);
				outErrorMetrics.close();
				for (int j = 0; j < target.length; j++) {
					outData.println((j+1)+" "+predicted[j]+" "+target[j]);
				}
				outData.close();
			}
		}
		return errorsallmodels;
	}
	
	public void printErrors(String directory, String errorMetric) {
		
		ArrayList<ArrayList<String>> errorsallmodels = this.printPredictedVsTarget_MD(directory, errorMetric);
		ArrayList<ArrayList<String>> errorsallmodels_0K = this.printPredictedVsTarget_0K(directory);
		PrintWriter outerrorsallmodels = null;
		try{			
			outerrorsallmodels = new PrintWriter(new BufferedWriter(new FileWriter(directory+"/errorsallmodels."+this.getDataSet().getTitle()+".data",true)));
		}catch(IOException e){
			Status.basic("Error writing!");
			e.printStackTrace();
		}
		for (int i = 0; i < errorsallmodels.size(); i++) {
			if(i < 1) {
				outerrorsallmodels.print("ModelComplexity,number_of_nodes,unitless,not_applicable,not_applicable ");
			}
			else {
				outerrorsallmodels.print(this.getEvaluators()[i-1].getComplexity()+" ");
			}
			for (String string : errorsallmodels.get(i)) {
				outerrorsallmodels.print(string+" ");
			}
			for (String string : errorsallmodels_0K.get(i)) {
				outerrorsallmodels.print(string+" ");
			}
			outerrorsallmodels.println();
		}
		outerrorsallmodels.close();
	}
	
	private ArrayList<ArrayList<String>> printPredictedVsTarget_0K(String directory) {

		this.getDataSet().setSubset("All");
		
		File directoryFile = new File(directory);
		directoryFile.mkdir();

		//Print the error of every evaluator with respect to every property in the data set
		String[] targetProperties = this.getDataSet().getTargetProperties();
		Arrays.sort(targetProperties);
		PotentialEvaluator[] evaluators = this.getEvaluators();

		//Array for a file of models (lines) and errors (columns)
		ArrayList<ArrayList<String>> errorsallmodels = new ArrayList<ArrayList<String>>();
		for (int i = 0; i < evaluators.length + 1; i++) {
			errorsallmodels.add(new ArrayList<String>());
		}
		for (String property : targetProperties) {
			boolean not_property_0k = property.equals("Energy") || property.equals("Force") || property.equals("Stress");
			if(not_property_0k) {
				continue;
			}
			double[] target = this.getDataSet().getTargetProperty(property);
			if(property.contains("Ev_f") || property.contains("Edumbbell_f")) {
				errorsallmodels.get(0).add(property+",predicted-target,meV,"+1000*target[0]+",meV");
			}
			else if(property.contains("delta_E0_")) {
				errorsallmodels.get(0).add(property+",predicted-target,meV/atom,"+1000*target[0]+",meV/atom");
			}
			else {
				String unitsStr = "";
				if(property.contains("a0_")) {
					unitsStr = "Angstrom";
				}
				else if(property.contains("C11_") || property.contains("C12_")|| property.contains("C44_")|| property.contains("BVRH_")) {
					unitsStr = "Angstrom";
				}
				else if(property.contains("Esurf_")) {
					unitsStr = "meV/(Angstrom^2)";
				}
				else {
					Status.basic("No units available for "+property+" !");
				}
				errorsallmodels.get(0).add(property+",%_error,unitless,"+target[0]+","+unitsStr);
			}
			for (int i = 0; i < evaluators.length; i++) {
				double[] predicted = evaluators[i].getPredictedProperty(property);
				double error;
				if(property.contains("delta_E0_") || property.contains("Ev_f") || property.contains("Edumbbell_f")) {
					error = 1000.0*(predicted[0]-target[0]);
				}			
				else {
					error = Statistics.getPercenErrors(target, predicted)[0];
				}
				errorsallmodels.get(i+1).add(Double.toString(error));
			}
		}
		
		//Print the maximum absolute % error of lattice parameter and elastic constants C11, C12, C44
		ArrayList<Integer> indicesECs = new ArrayList<Integer>();
		//Print the mean absolute error of surface energies
		ArrayList<Integer> indicesSurf = new ArrayList<Integer>();
		int i = 0;
		for (String string : errorsallmodels.get(0)) {
			if(string.contains("C11_") || string.contains("C12_") || string.contains("C44_")) {
				indicesECs.add(i);
			}
			else if(string.contains("Esurf_")) {
				indicesSurf.add(i);
			}
			i++;
		}
		int count = 0;
		for (ArrayList<String> list : errorsallmodels) {
			if(count < 1) {
				list.add("max_abs_percent_error_elastic_constants,max(abs(%_errors)),unitless,not_applicable,not_applicable");
				list.add("mean_abs_percent_error_surface_energies,mean(abs(%_errors)),unitless,not_applicable,not_applicable");
			}
			else {
				ArrayList<Double> errorsECs = new ArrayList<Double>();
				for (Integer integer : indicesECs) {
					errorsECs.add(Math.abs(Double.parseDouble(list.get(integer))));
				}	
				double[] errorsArrayECs = new double[errorsECs.size()];
				for (int j = 0; j < errorsArrayECs.length; j++) {
					errorsArrayECs[j] = errorsECs.get(j);
				}
				list.add(Double.toString(Statistics.getMaxValue(errorsArrayECs)));
				
				ArrayList<Double> errors = new ArrayList<Double>();
				for (Integer integer : indicesSurf) {
					errors.add(Math.abs(Double.parseDouble(list.get(integer))));
				}	
				double[] errorsArray = new double[errors.size()];
				for (int j = 0; j < errorsArray.length; j++) {
					errorsArray[j] = errors.get(j);
				}
				list.add(Double.toString(Statistics.getMean(errorsArray)));

			}
			count++;
		}

		return errorsallmodels;
	}
	
	/**
	 * For this method, every evaluator in the population corresponds to the fittest model in each iteration (or snapshot)
	 * @param fileNameAbsolutePath the name of the file including the absolute path
	 * @param errorMetric
	 */
	public void printErrorsVsIteration(String fileNameAbsolutePath, String errorMetric, ArrayList<String> iterationTime) {
		this.getDataSet().setSubset("All");
		
		PrintWriter out = null;
		try{			
			out = new PrintWriter(new BufferedWriter(new FileWriter(fileNameAbsolutePath)));
		}catch(IOException e){
			Status.basic("Error writing!");
			e.printStackTrace();
		}		
		
		//Print the error of every evaluator with respect to every property in the data set
		String[] targetProperties = this.getDataSet().getTargetProperties();
		
		//Store the errors of each model to save time for repeated models
		HashMap<String, double[]> map = new HashMap<String, double[]>();
		
		out.print("iteration time(hours) ");
		for (String string : targetProperties) {
			out.print(string+" ");
		}
		out.println();
		
		PotentialEvaluator[] evaluators = this.getEvaluators();
		for (int i = 0; i < evaluators.length; i++) {
			out.print(iterationTime.get(i)+" ");
			
			double[] errors = map.get(evaluators[i].getExpression());
			if(errors == null) {
				errors = new double[targetProperties.length];
				int c = 0;
				for (String property : targetProperties) {
					double[] target = this.getDataSet().getTargetProperty(property);				
					double[] predicted = evaluators[i].getPredictedProperty(property);
					if(property.equals("Energy")) {
						double minTarget = Statistics.getMinValue(target);
						double minPredicted = Statistics.getMinValue(predicted);
						target = Statistics.arrayAdd(target, -minTarget);
						predicted = Statistics.arrayAdd(predicted, -minPredicted);
					}
					double error = Double.NaN;
					if(errorMetric.equals("MAE")) {
						error = Statistics.getMAE(target, predicted);
					}
					else {
						new Exception().printStackTrace();
						Status.basic("The error metric "+errorMetric+" is not available!");
					}
					errors[c] = error;
					c++;
				}
				map.put(evaluators[i].getExpression(), errors);
			}
			for (int j = 0; j < errors.length; j++) {
				out.print(errors[j]+" ");
			}
			out.println();
		}
		out.close();
	}
	
	private PotentialEvaluator[] getEvaluators() {
		if(m_Evaluators==null) {
			this.setEvaluators(m_PopulationFile);
		}
		return m_Evaluators;
	}
	
	private DataSet getDataSet() {
		return m_DataSet;
	}
	
	/**
	 * get the models from a file
	 * @param filePath
	 * @return
	 */
	private void setEvaluators(String filePath) {		
		ArrayList<PotentialEvaluator> individuals = new ArrayList<PotentialEvaluator>();
		try{
			BufferedReader br = new BufferedReader(new FileReader(filePath));
			String line;
			while((line = br.readLine())!= null){
				String[] row = line.split(" ");
				PotentialEvaluator eval = new PotentialEvaluator(TreePotential.buildTree(row[4], Main.m_CutoffDistance), this.getDataSet());
				individuals.add(eval);
			}
			br.close();
		}catch(IOException e){
			e.printStackTrace();
		}
		PotentialEvaluator[] evaluators = new PotentialEvaluator[individuals.size()];
		for (int i = 0; i < evaluators.length; i++) {
			evaluators[i] = individuals.get(i);
		}
		m_Evaluators = evaluators;
	}
	

}